from flask import Flask, session, redirect, url_for, request, render_template
import database.mongo_db as db
app = Flask(__name__, template_folder='templates')
app.secret_key = "WDWKJN-aspeng-smen-15748523"


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/getdata')
def get_data():
    return "Hello from getdata"


@app.route('/getdata/<id>')
def get_data_id(id):
    i = float(id)
    if i > 15:
        return f"Hello from getdata {id}"
    return "id < 15"


@app.route('/getdata/<id1>/<id2>', methods = ['GET'])
def get_data_sum(id1, id2):
    return str(float(id1)+float(id2))


@app.route('/getdata/<id1>/<id2>', methods = ['POST'])
def get_data_sub(id1, id2):
    return str(float(id1)-float(id2))


@app.route('/gethtmldata/<id1>/<id2>', methods = ['GET'])
def get_htmldata_sum(id1, id2):
    sum = str(float(id1)+float(id2))
    return render_template("get_sum.html", value=sum)


@app.route('/chart', methods = ['GET'])
def get_chart():
    axis, data = db.get_one_trend()
    return render_template("chart_l.html", data=data, axis=axis)


@app.route('/chart/<id>', methods = ['GET'])
def get_chart_id(id):
    axis, data = db.get_trend_id(id)
    return render_template("chart_l.html", data=data, axis=axis)


@app.route('/api/getusers', methods = ['GET','POST'])
def get_users():
    return db.get_json_users()


@app.route('/api/getusers/<age>', methods = ['GET','POST'])
def get_users_age(age):
    return db.get_json_users_with_age(age=int(age))


@app.route('/charts')
def charts():
    ids = db.get_trend_ids()
    return render_template("charts.html", ids=ids, len=len(ids))


if __name__ == '__main__':
    app.run()
