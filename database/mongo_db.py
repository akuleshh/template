from pymongo import MongoClient
import json
from bson.objectid import ObjectId

MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
DB_NAME = 'test1'
COLLECTION_NAME = 'users'


class Encoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        else:
            return obj



def get_all_users(age):
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME][COLLECTION_NAME]
    users = collection.find({"age": age})
    for user in users:
        print(user)
    connection.close()


def add_user():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME][COLLECTION_NAME]
    user = {"name": "Jon", "age": 20}
    collection.insert_one(user)
    connection.close()


def get_json_users():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME][COLLECTION_NAME]
    users = collection.find()
    result = [x for x in users]
    connection.close()
    return json.dumps(result, cls=Encoder)


def get_json_users_with_age(age):
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME][COLLECTION_NAME]
    users = collection.find({"age": age})
    result = [x for x in users]
    connection.close()
    return json.dumps(result, cls=Encoder)


def get_users_name():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME][COLLECTION_NAME]
    users = collection.find()
    result = [{"name": user["name"], "age": user["age"]} for user in users]
    connection.close()
    return json.dumps(result, cls=Encoder)


def get_all_trend():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME]["trend"]
    trends = collection.find()
    for trend in trends:
        print(trend["data"])
    connection.close()


def get_one_trend():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME]["trend"]
    trend = collection.find_one()
    print(trend["data"])
    x = []
    y = []
    data = trend["data"]
    for point in data:
        x.append(float(point))
        y.append(data[point])
    connection.close()
    return x, y


def get_trend_id(id):
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME]["trend"]
    trend = collection.find_one({"_id": ObjectId(id)})
    print(trend["data"])
    x = []
    y = []
    data = trend["data"]
    for point in data:
        x.append(float(point))
        y.append(data[point])
    connection.close()
    return x, y


def get_trend_ids():
    connection = MongoClient(MONGODB_HOST, MONGODB_PORT)
    collection = connection[DB_NAME]["trend"]
    trends = collection.find()
    result = [str(trend["_id"]) for trend in trends]
    connection.close()
    return result #json.dumps(result)


if __name__ == "__main__":
    # add_user()
    # get_all_users(20)
    # get_all_trend()
    # x, y = get_one_trend()
    x, y = get_trend_id(id="617bf317f659f75086bdc1d3")
    print(x, y)
    print(get_json_users())
    print(get_users_name())
    print(get_trend_ids())

